# SAE-systeme

https://gitlab.com/Juliette0070/sae-systeme

J’ai choisi d’installer une machine virtuelle sur mon ordinateur car je voulais garder mon PC sur Windows. Pour cela j’ai téléchargé « VMware Workstation 16 Player ». J’ai créé une nouvelle machine avec 60Go de stockage, 4Go de RAM et j’ai utilisé xubuntu comme image (fichier disponible avec les fichiers de configuration) (j’ai laissé le reste des options par défaut). Pour télécharger VSCode et ses extensions (python, autodocstring, pytest runner et gitlab workflow), python, java et docker, j’ai écrit et utilisé le script.txt disponible avec les fichiers de configuration puis j’ai configuré Oracle à la main en exécutant les commandes disponibles dans le fichier oracle.txt (en me basant sur l’aide dans le cours de base de données sur CELENE). J’ai également lié mon compte gitlab.
